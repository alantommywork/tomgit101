// map

const arr1 = [1,2,3,4,5,6];

// console.log(arr1.map(element => -element));

//filter จำนวนที่มากกว่า 2

// console.log(arr1.filter(element => element >2));

const forEach = arr1.forEach(element => {

    console.log(element);

});

console.log('================');

const map = arr1.map(element => {

    return element + 1;

});

// console.log(map);

const filter = arr1.filter(element => {

    return element >= 4;

});

// console.log(filter);

const mixMapFilter = arr1

.map(element => {

    return element + 1;

})

.filter(element => {

    return element > 2;

});

console.log(mixMapFilter);

const find = arr1.find(element => element > 3);

console.log(find);

// normal if

if (a == b) {

    console.log('Hello');

} else {

    console.log('Hi');

}

// ลบ if ออก

// เอาเงื่อนไขแรกตั้ง คั้นด้วย ?

// ใส่ ของที่อยู่ในเงื่อนไขแรก

// คั้นด้วย : ตามด้วยของที่อยู่ในเงื่อนไขที่สอง

// short if

a == b ? console.log('Hello') : console.log('Hi');